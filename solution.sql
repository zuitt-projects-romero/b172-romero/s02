
-- Create a database.
CREATE DATABASE enrollment_db;

-- Select a database.
USE enrollment_db;

-- Create tables
-- Table columns have the following format: [column_name] [data_type] [other_options]
CREATE TABLE students (
	id INT NOT NULL AUTO_INCREMENT,
    student_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE teachers (
    id INT NOT NULL AUTO_INCREMENT,
    teacher_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE courses (
    id INT NOT NULL AUTO_INCREMENT,
    course_name VARCHAR(50) NOT NULL,
    teacher_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_courses_teacher_id
        FOREIGN KEY (teacher_id) REFERENCES teachers(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);



CREATE TABLE student_courses (
    id INT NOT NULL AUTO_INCREMENT,
    course_id INT NOT NULL,
    student_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_student_courses_course_id
        FOREIGN KEY (course_id) REFERENCES courses(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk__student_courses_student_id
        FOREIGN KEY (student_id) REFERENCES students(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

-- CONSTRAINT option -> used to specify rules for the data in a table. Constraints are used to limit the type of data that can go into a table.

-- UPDATE CASCADE -> used by the referencing rows that are updated in the child table when the referenced row is updated in the parent table which has a primary key.

-- DELETE CASCADE -> deletes the referencing rows in the child table when the referenced row is deleted in the parent table which has a primary key.
